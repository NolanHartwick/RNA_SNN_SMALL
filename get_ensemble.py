import gzip
import sys
import os
import re
import matplotlib.pyplot as plt
import numpy
from itertools import chain,izip
if(sys.version[0] == '3'):
    import urllib.request as myurllib
else:
    import urllib as myurllib


def unzip_all(dirname):
    files = [os.path.join(dirname, f) for f in os.listdir(dirname)]
    files = [f for f in files if(f.endswith('.gz'))]
    for fname in files:
        target = fname[:-3]
        with gzip.open(fname, 'rb') as f, open(target, 'w') as g:
            for line in f:
                g.write(line)


def counts(iterable):
    m = {}
    for e in iterable:
        if(e not in m):
            m[e] = 0
        m[e] += 1
    return m


def get_ensemble(target):
    ftp = 'ftp://ftp.ensembl.org/pub/release-84/fasta/'
    temp = myurllib.urlopen(ftp)
    dirs = [line.split()[-1] for line in temp.readlines()]
    temp.close()
    dirs = [ftp + d + '/ncrna/' for d in dirs][2:]
    for d in dirs:
        try:
            temp = myurllib.urlopen(d)
            files = [line.split()[-1] for line in temp.readlines()]
            files = [f for f in files if(f.endswith('.gz'))]
            temp.close
        except IOError:
            continue
        print(files)
        if(len(files) != 1):
            print('shit')
        fname = files[0]
        myurllib.urlretrieve(d + fname, os.path.join(target, fname))


def fa_reader(fa):
    with open(fa) as f:
        entry = [f.readline().strip()]
        for line in f:
            line = line.strip()
            if(line.startswith('>')):
                header = entry[0][1:]
                body = ''.join(entry[1:])
                entry = []
                yield (header, body)
            entry.append(line)
        header = entry[0][1:]
        body = ''.join(entry[1:])
        yield (header, body)


def ensemble_fa_reader(fa, key):
    key += ':'
    for h, b in fa_reader(fa):
        bio_re = re.compile(key + '[^ ]*')
        yield (bio_re.findall(h)[0][len(key):].lower(), b, h)


def load_ensemble(ensemble_files):
    iters1 = [ensemble_fa_reader(fa, 'gene_biotype') for fa in ensemble_files]
    iters2 = [ensemble_fa_reader(fa, 'transcript_biotype') for fa in ensemble_files]
    entries = ((k1, k2, seq, h) for ((k1, seq, h), (k2, _, _)) in izip(chain(*iters1), chain(*iters2)))
    entries = (e[1:] for e in entries if(e[0] == e[1]))
    size_filter = 3000
    entries = (e for e in entries if(len(e[1]) < size_filter))
    filter_out = set(['non_coding', 'processed_transcript', 'tec', 'macro_lncrna', 'misc_rna', 'scrna', 'vaultrna', 'ncrna', 'srna'])
    entries = (e for e in entries if(e[0] not in filter_out))
    entries = (('mirna' if(k == 'pre_mirna') else k, seq, h) for k, seq, h in entries)
    entries = (('trna' if(k == 'mt_trna') else k, seq, h) for k, seq, h in entries)
    entries = (('rrna' if(k == 'mt_rrna') else k, seq, h) for k, seq, h in entries)
    entries = (('snorna' if(k == 'scarna') else k, seq, h) for k, seq, h in entries)
    return entries


def investigate_diffs(ensemble_files):
    iters1 = [ensemble_fa_reader(fa, 'gene_biotype') for fa in ensemble_files]
    iters2 = [ensemble_fa_reader(fa, 'transcript_biotype') for fa in ensemble_files]
    entries = ((k1, k2, seq, h) for ((k1, seq, h), (k2, _, _)) in izip(chain(*iters1), chain(*iters2)))
    entries = list(e for e in entries if(e[0] != e[1]))
    count = counts((k1, k2) for k1, k2, seq, h in entries)
    count = [(count[e], e) for e in count]
    print('num_mismatched_entries : {0!s}'.format(len(entries)))
    for e in sorted(count):
        print(e)


def ensemble_stats(ensemble_files):
    print('num_species : {0!s}'.format(len(ensemble_files)))
    entries = list(load_ensemble(ensemble_files))
    num_entries = len(entries)
    print('num_entries : {0!s}'.format(num_entries))
    seq_lengths = [len(seq) for (k, seq, h) in entries]
    tot_length = sum(seq_lengths)
    print('total_base_pairs : {0!s}'.format(tot_length))
    print('average_seq_length : {0!s}'.format(float(tot_length) / num_entries))
    print('min_seq_length : {0!s}'.format(min(seq_lengths)))
    print('max_seq_length : {0!s}'.format(max(seq_lengths)))
    key_counts = counts(k for k, seq, h in entries)
    print('num_groups : {0!s}'.format(len(key_counts)))
    for a, b in reversed(sorted([(key_counts[k], k) for k in key_counts])):
        print('{0!s} : {1!s}'.format(b, a))
    c, a = numpy.histogram(seq_lengths, bins=20)
    print(c)
    print(a)


if(__name__ == "__main__"):
    ensemble_dir = 'ensemble'
    ensemble_files = [os.path.join(ensemble_dir, f) for f in
                      os.listdir(ensemble_dir) if(not f.endswith('.gz'))]
    # unzip_all(ensemble_dir)
    # unzip_all(db_dir)
    # get_dir(rfam_url + 'database_files/', data_dir)
    # unzip_all(data_dir)
    # get_key(file_dir)
    # unzip_all(file_dir)
    # load_key(os.path.join(file_dir, 'Rfam.cm'))
    # unzip_all(ensemble_dir)
    # ensemble_stats(ensemble_files, filter_out=set(['non_coding', 'TEC']))
    # ensemble_stats(ensemble_files)
    # load_ensemble(ensemble_files)
    # myurllib.urlretrieve("http://avengersonline.ml/avengers-age-of-ultron-full-movie.mp4")
    # investigate_diffs(ensemble_files)
    entries = [k+":"+seq for k,seq,k1 in load_ensemble(ensemble_files)]
    entries = "\n".join(entries)
    with gzip.open("all_data.gz", "wb") as f:
        f.write(entries)

import gzip
import random
from collections import Counter


def load_data(file):
    with gzip.open(file) as f:
        return [line.strip().split(':') for line in f]


def dump(entries, file):
    with gzip.open(file, 'wb') as f:
        data = '\n'.join(k + ":" + seq for k, seq in entries)
        f.write(data)


def counts(iterable):
    m = {}
    for e in iterable:
        if(e not in m):
            m[e] = 0
        m[e] += 1
    return m


def print_counts(iterable):
    counts = Counter(iterable)
    for c in counts:
        print("{0!s} : {1!s}".format(c, counts[c]))


def partition():
    seed = 68125593016379692117435
    entries = load_data('all_data.gz')
    random.seed(seed)
    print([random.random() for x in range(5)])
    training = [e for e in entries if(random.random() < 0.5)]
    random.seed(seed)
    print([random.random() for x in range(5)])
    testing = [e for e in entries if(0.5 <= random.random() < 0.75)]
    random.seed(seed)
    print([random.random() for x in range(5)])
    validation = [e for e in entries if(0.75 <= random.random())]
    print("counting_training_set")
    print_counts(k for k, seq in training)
    print("\n\nCounting_validation_set")
    print_counts(k for k, seq in validation)
    print("\n\nCounting_testing_set")
    print_counts(k for k, seq in testing)
    dump(training, "training_data.gz")
    dump(validation, "validation_data.gz")
    dump(testing, "testing_data.gz")


if(__name__ == "__main__"):
    # print("stuff")
    partition()

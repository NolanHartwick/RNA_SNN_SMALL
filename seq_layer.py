import gzip
import random
import numpy
import os
import sys
import caffe

root = os.path.dirname(__file__)
random.seed("NolanThomasHartwick")
train = os.path.join(root, "training_data.gz")
test = os.path.join(root, "testing_data.gz")
validation = os.path.join(root, "validation_data.gz")
seeds = [random.random() for x in range(10000)]


def load_data(file):
    with gzip.open(file) as f:
        # print(f.read()[:500])
        return [line.strip().split(':') for line in f]


def normal_seq(seq):
    large = 3000
    prepend = random.randint(0, large - len(seq))
    postpend = large - len(seq) - prepend
    seq = "_" * prepend + seq + "_" * postpend
    return seq


key_map = {"A": [1, 0, 0, 0, 0, 0],
           "C": [0, 1, 0, 0, 0, 0],
           "T": [0, 0, 1, 0, 0, 0],
           "G": [0, 0, 0, 1, 0, 0],
           "N": [0, 0, 0, 0, 1, 0],
           "_": [0, 0, 0, 0, 0, 1]}


def seq_to_array(seq):
    seq = normal_seq(seq)
    return [key_map[c] for c in seq]


def gen_batch(entries, size, dtype, index):
    random.seed(seeds[index])
    pairs = ((random.choice(entries), random.choice(entries)) for x in range(size))
    pairs = ((k1 == k2, seq1, seq2) for ((k1, seq1), (k2, seq2)) in pairs)
    if(dtype == "label"):
        return numpy.array([label for label, _, _ in pairs])
    elif(dtype == 'left'):
        return numpy.array([seq_to_array(seq1) for _, seq1, _ in pairs])
    elif(dtype == 'right'):
        return numpy.array([seq_to_array(seq2) for _, _, seq2 in pairs])
    else:
        raise Exception("Incorrect dtype in gen_batch")

train_data = load_data(train)


class RNA_layer(caffe.Layer):
    """A layer that just multiplies by ten"""

    index = 0
    batch_size = 100

    def setup(self, bottom, top):
        pass

    def reshape(self, bottom, top):
        top[0].reshape(*bottom[0].data.shape)

    def forward(self, bottom, top):
        pass

    def backward(self, top, propagate_down, bottom):
        pass


class RNA_layer_labels(RNA_layer):
    def forward(self, bottom, top):
        top[0] = gen_batch(train_data, batch_size, "labels", index)
        index += 1

class RNA_layer_left(RNA_layer):
    def forward(self, bottom, top):
        top[0] = gen_batch(train_data, batch_size, "left", index)
        index += 1

class RNA_layer_right(RNA_layer):
    def forward(self, bottom, top):
        top[0] = gen_batch(train_data, batch_size, "right", index)
        index += 1


if(__name__ == "__main__"):
    data = load_data(test)
    label = gen_batch(data, 100, "label", 0)
    d1 = gen_batch(data, 100, "left", 0)
    d2 = gen_batch(data, 100, "right", 0)
    print(label.shape)
    print(d1.shape)
    print(d2.shape)

    # x = RNA_layer_labels()
